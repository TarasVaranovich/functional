package carrying

import immutability.FunctionalObject

object Carrying {

  def main(args: Array[String]): Unit = {

    val firstObject = new FunctionalObject(1, "first object")
    val secondObject = new FunctionalObject(2, "second object")


    def multiplyCounter(firstTerm: FunctionalObject, secondTerm: FunctionalObject): Int = {
      firstTerm.counter_ * secondTerm.counter_
    }

    val notCurriedFunction = multiplyCounter(firstObject, secondObject)
    println("Not curried function execution result:")
    println(notCurriedFunction)

    def curriedMultiplyCounter(firstTerm: FunctionalObject)(secondTerm: FunctionalObject): Int = {
      firstTerm.counter_ * secondTerm.counter_
    }

    def dividedMultiplyCounter(firstTerm: FunctionalObject) = (secondTerm: FunctionalObject)
    => firstTerm.counter_ * secondTerm.counter_

    val curriedFunction = curriedMultiplyCounter(firstObject)(secondObject)
    println("Curried function execution result:")
    println(curriedFunction)

    val firstPartOfCurriedFunction = curriedMultiplyCounter(firstObject)_
    val secondPartOfCurriedFunction = firstPartOfCurriedFunction(secondObject)
    println("Curried function partially execution result:")
    println(secondPartOfCurriedFunction)

    val firstPartOfDividedFunction = dividedMultiplyCounter(firstObject)
    val secondPartOfDividedFunction = firstPartOfDividedFunction(secondObject)
    println("Divided function partially execution result:")
    println(secondPartOfDividedFunction)
  }
}
