package case_classes

/**
 * Class represents usage of case-classes and wildcard pattern
 */
object CaseClassesRunner {

  def main(args: Array[String]): Unit = {

    println("Constant wildcard examples:")
    println(WildCardHolder.constantWildCard(1))
    println(WildCardHolder.constantWildCard(true))
    println(WildCardHolder.constantWildCard("true"))
    println(WildCardHolder.constantWildCard(Nil))

    println("Variable wildcard examples:")
    println(WildCardHolder.variableWildCard(1))
    println(WildCardHolder.variableWildCard(2))
    println(WildCardHolder.variableWildCard(3))

    println("Sequence wildcard examples:")
    println(WildCardHolder.sequenceWildCard(List(0, 1, 2, 3)))
    println(WildCardHolder.sequenceWildCard(List(4, 0, 2, 3, 6)))
    println(WildCardHolder.sequenceWildCard(List(1, 7, 2, 3)))
    println(WildCardHolder.sequenceWildCard(List(11, 7, 3, 5)))
    println(WildCardHolder.sequenceWildCard(List(9, 17, 32, 45)))

    println("Constructor wildcard examples:")
    val car = Car(1)
    println(WildCardHolder.constructorWildCard(car))
    val truck = Truck("brick", 2)
    println(WildCardHolder.constructorWildCard(truck))
    val heavyTruck = Truck("gasoline", 10)
    println(WildCardHolder.constructorWildCard(heavyTruck))

    try {
      val emptyCar = Car(0)
      println(WildCardHolder.constructorWildCard(emptyCar))
    } catch {
      case _: MatchError => println("No appropriate case in match statement.")
      case e: Throwable => println(e.getMessage)
    }

    println("Typed wildcard examples:")
    val str = "example"
    println(String.format("String with length '%d'", WildCardHolder.typedWildCard(str)))
    val list = List("a", "b", "c")
    println(String.format("List with length '%d'", WildCardHolder.typedWildCard(list)))
    println(String.format("Integer with value '%d'", WildCardHolder.typedWildCard(23)))
  }
}
