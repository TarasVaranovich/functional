package case_classes

case class Truck(cargoType: String, cargoWeight: Double) extends Vehicle
