package case_classes

object WildCardHolder {

  def constantWildCard(constant: Any): String = constant match {
    case 1 => "Yes"
    case true => "Yes"
    case "true" => "Yes"
    case _ => "No"
  }

  /**
   * Variable wildcard which terminated by universal wildcard
   * @param variable
   * @return
   */
  def variableWildCard(variable: Int): String = variable match {
    case 1 => "One"
    case 2 => "Two"
    case _ => "Unknown"
  }

  def sequenceWildCard(list: List[Int]): String = list match {
    case List(0, _*) => "List starts from 'zero'"
    case List(_, 0, _*) => "Zero at second position"
    case List(1, _*) => "List starts from 'one'"
    case List(_, _, 3, _*) => "At third position of list is 'three'"
    case List(_*) => "There is nothing specials in list"
  }

  /**
   * Constructor wild card additionally provides a 'deep match'
    *
   * @param vehicle
   * @return
   */
  def constructorWildCard(vehicle: Vehicle): String = vehicle match {
    case Car(1) => "Car with only driver inside"
    case Truck("brick", 2) => "Truck with two tones of bricks"
    case Truck(_, 10) => String.format("Truck with ten tones of '%s'", vehicle.asInstanceOf[Truck].cargoType)
  }

  def typedWildCard(obj: Any): Int = obj match {
    case string: String => string.length
    case list: List[_] => list.length
    case integer: Int => integer
  }
}
