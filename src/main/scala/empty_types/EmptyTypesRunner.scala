package empty_types

object EmptyTypesRunner extends App {

  val list = List.empty
  println("Nil represents empty collection: " + (list == Nil))

  val optional = Option.empty
  println("None represents empty optional: " + (optional == None))

  val emptyStringList: List[String] = List[Nothing]()
  val emptyIntList: List[Int] = List[Nothing]()
  //doesn't work
  //val emptyStringList: List[String] = List[Nothing]("abc")

}
