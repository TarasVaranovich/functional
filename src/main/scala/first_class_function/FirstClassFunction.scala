package first_class_function

import immutability.FunctionalObject
import pure_function.FunctionsHandler

object FirstClassFunction {

  def main(args: Array[String]): Unit = {

    def incrementPureCounter(incrementSize: Int, changedObject: FunctionalObject): FunctionalObject = {
      new FunctionalObject(changedObject.counter_ + incrementSize, changedObject.message_)
    }

    def multiplyCounter(prime: Int, changedObject: FunctionalObject) = {
      new FunctionalObject(changedObject.counter_ * prime, changedObject.message_)
    }

    val firstObject = new FunctionalObject(1, "first object")
    val secondObject = new FunctionalObject(2, "second object")

    val incrementalSummarize = FunctionsHandler.performFirstClassFunction(incrementPureCounter)
    val multipliedSummarize = FunctionsHandler.performFirstClassFunction(multiplyCounter)

    val incrementalResult = incrementalSummarize.apply(firstObject, secondObject)
    val multipliedResult = multipliedSummarize.apply(firstObject, secondObject)

    println(incrementalResult)
    println(multipliedResult)
  }
}
