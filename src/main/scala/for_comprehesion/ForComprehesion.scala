package for_comprehesion

import for_comprehesion.functions.{foo, sideEffectFoo}

//every datatype supports map, flatMap, withFilter can be used in for comprehesion
object ForComprehesion extends App {
  val userBase = List(User("Travis", 28), User("Kelly", 33), User("Jennifer", 44), User("Dennis", 23))

  //generates result which container type is the same (List)
  val twentySomethings =
    for (user <- userBase if user.age >= 20 && user.age < 30)
      yield user.name

  twentySomethings.foreach(println)

  foo(10, 10) foreach {
    case (i, j) => println(s"($i, $j) ")
  }

  sideEffectFoo(10, 10)
}
