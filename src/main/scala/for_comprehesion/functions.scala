package for_comprehesion

package object functions {

  def foo(n: Int, v: Int) = for (i <- 0 until n; j <- 0 until n if i + j == v) yield (i, j)

  def sideEffectFoo(n: Int, v: Int) = for (i <- 0 until n; j <- 2 until 5) println(s"$i, $j")
}
