package functor

class FunctionalObjectExtended(counter: Int, message: String, isCreated: Boolean) {

  override def toString: String =
    String.format("FunctionalObjectExtended { counter = %d, message = %s, isCreated = %b }",
      counter, message, isCreated)
}
