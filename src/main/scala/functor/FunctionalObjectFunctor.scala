package functor

class FunctionalObjectFunctor[T](obj: T)

  extends Functor[T] {

  override def map[R](f: T => R): Functor[R] = new FunctionalObjectFunctor[R](f.apply(obj))

}
