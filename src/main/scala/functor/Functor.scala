package functor

/**
 * Functor is category type which maps entities of one type
 * to entities of another type
 *
 * @tparam T
 */
trait Functor[T] {
  /**
   * covariant functor.
   *
   * @param f
   * @tparam R
   * @return
   */
  def map[R](f: T => R): Functor[R]
}
