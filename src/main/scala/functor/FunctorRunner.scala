package functor

import immutability.FunctionalObject

object FunctorRunner {

  def main(args: Array[String]): Unit = {

    val firstFunctionalObject = new FunctionalObject(1, "first")
    val functionalObjectFunctor = new FunctionalObjectFunctor[FunctionalObject](firstFunctionalObject)

    functionalObjectFunctor
      .map(
        (obj: FunctionalObject) => {
          obj.counter_ + obj.message_.length
        })
      .map((value: Int) => {
        new FunctionalObjectExtended(value, "extended object", true)
      })
      .map((obj: FunctionalObjectExtended) => {
        println(obj)
      })
  }
}
