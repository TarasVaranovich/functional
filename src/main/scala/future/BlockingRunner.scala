package future

import java.util.concurrent.Executors
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future, blocking}
import scala.language.postfixOps

object BlockingRunner extends App {

  implicit val executionContext = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(4))

  val blockingInsideFuture = Future {
    println(devideTen(3))
    blocking {
      Thread.sleep(1000)
    }
  }

  //Await.result(blockingInsideFuture, 2 seconds)
}
