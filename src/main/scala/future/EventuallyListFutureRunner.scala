package future

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, Future}
import scala.language.postfixOps

object EventuallyListFutureRunner extends App {
  val listFuture: Future[List[Int]] = Future {
    listFunction(10)
  }

  listFuture.foreach({
    list => {
      for (value <- list) println(s"First loop $value")
    }
  })

  listFuture.foreach({
    list => {
      for (value <- list) println(s"Second loop $value")
    }
  })

  Await.result(listFuture, 2 seconds)
}
