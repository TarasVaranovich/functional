package future

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, Future}
import scala.language.postfixOps

object FunctionalComposition extends App {

  val first: Future[Int] = Future {
    getRandomInt(10)
  }

  val next: Future[Int] = Future {
    getRandomInt(10)
  }

  val firstComposition = for {
    firstRes <- first
    nextRes <- next
    if (isDirectOrder(firstRes, nextRes))
  } yield println(s"1: Sum of $firstRes and $nextRes is: " + sum(firstRes, nextRes))

  Await.result(firstComposition, 2 seconds)

  val secondComposition = first
    .flatMap(firstRes => next
      .withFilter(nextRes => isDirectOrder(firstRes, nextRes))
      .map(nextRes => println(s"2: Sum of $firstRes and $nextRes is: " + sum(firstRes, nextRes)))
    )

  Await.result(secondComposition, 2 seconds)
}
