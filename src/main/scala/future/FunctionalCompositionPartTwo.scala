package future

import scala.concurrent.{Await, Future}
import scala.language.postfixOps
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import scala.util.Success

object FunctionalCompositionPartTwo extends App {

  val devisionFuture: Future[Double] = Future {
    devideTen(0)
  }

  val recovered: Future[Unit] = devisionFuture map {
    result => println(result)
  } recover {
    case _: ArithmeticException => println("Deveision by zero")
  }

  Await.result(recovered, 2 seconds)

  val successfulDevisionFuture: Future[Unit] = Future {
    val res = devideTen(3)
    println(res)
  }

  val resolvedFuture = devisionFuture fallbackTo successfulDevisionFuture


  Await.result(resolvedFuture, 2 seconds)

  val compositionFuture = Future {
    devideTen(5)
  } andThen {
    case Success(value) => println(value)
  } andThen {
    value =>
      println(s"Side effect of result: $value")
  }

  Await.result(compositionFuture, 2 seconds)
}
