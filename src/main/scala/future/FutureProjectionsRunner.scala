package future

import scala.language.postfixOps
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object FutureProjectionsRunner extends App {

  val failedFuture = Future {
    delayedFunction(0)
  }

  for (ex <- failedFuture.failed) println(ex)

  failedFuture.failed.foreach(ex => println(ex))
}
