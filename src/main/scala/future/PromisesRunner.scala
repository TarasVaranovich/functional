package future

import scala.language.postfixOps
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, Future, Promise}

object PromisesRunner extends App {

  val promise = Promise[Int]()
  val future = promise.future

  val producer = Future {
    val result = sum(2, 5)
    promise success result
    println("Do something unrelated")
  }

  val consumer = Future {
    println("Do something before")
    future foreach {
      result => {
        val devide = devideTen(result)
        println(devide)
      }
    }
  }

  val failureProducer = Future {
    val result = devideTen(1)
    if (result < 20) promise failure (new IllegalStateException)
    else {
      val nextResult = sum(result.intValue(), 5)
      promise success (nextResult)
    }
  }

  Await.result(consumer, 2 seconds)

  val secondFuture = Future {
    1
  }
  val secondPromise = Promise[Int]()

  secondPromise completeWith secondFuture

  secondPromise.future.foreach {
    result => println(result)
  }

  val firstComposition = Future[Int] {
    24
  }
  val secondComposition = Future[Int] {
    45
  }
  val promiseComposed = Promise[Int]
  firstComposition foreach {
   result => promiseComposed.trySuccess(result)
  }
  secondComposition foreach {
    result => promiseComposed.trySuccess(result)
  }
  promiseComposed.future.foreach {
    result => println("Composed either:" + result)
  }

  Await.result(failureProducer, 2 seconds)
}
