package future

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}
import scala.language.postfixOps

object SimpleFutureRunner extends App {
  println(Runtime.getRuntime.availableProcessors())

  val delayedFuture: Future[String] = Future {
    delayedFunction(0)
  }

  delayedFuture.onComplete({
    case Success(result) => println("Result:" + result)
    case Failure(error) => println("Error:" + error)
  })

  Await.result(delayedFuture, 2 seconds)
}
