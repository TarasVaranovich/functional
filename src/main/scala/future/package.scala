import scala.util.Random

package object future {

  def delayedFunction(delay: Int): String = {
    Thread.sleep(delay);
    s"Delayed $delay millis:" + (1000 / delay)
  }

  def listFunction(count: Int): List[Int] = {
    List.tabulate(count)(n => n * n)
  }

  def getRandomInt(rangeBorder: Int): Int = {
    Random.nextInt(rangeBorder)
  }

  def isDirectOrder(previous: Int, next: Int): Boolean = {
    previous < next
  }

  def sum(previous: Int, next: Int): Int = {
    previous + next
  }

  def devideTen(devisor: Int): Double = {
    10 / devisor
  }
}
