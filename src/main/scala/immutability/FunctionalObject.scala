package immutability

class FunctionalObject(counter: Int, message: String) {

  override def toString: String =
    String.format("FunctionalObject { counter = %d, message = %s }", counter, message)

  def counter_ = counter;

  def message_ = message;
}
