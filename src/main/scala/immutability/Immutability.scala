package immutability

object Immutability {

  def main(args: Array[String]): Unit = {
    val firstObject = new FunctionalObject(1, "first object")
    var secondObject = new FunctionalObject(2, "second object")

    //firstObject.counter

    println("Created objects:")
    println(firstObject)
    println(secondObject)

    println("Objects after reassignment:")
    //firstObject = secondObject
    secondObject = firstObject
    println(firstObject)
    println(secondObject)
  }
}

