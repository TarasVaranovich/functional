package monad

/**
 * Monad is container which contains value and context
 * Monad provides ability to perform complicated sequence of functions/sequential computations
 * monad use result from previous monad as a parameter
 * Monad is category
 *
 * @tparam T
 */
trait Monad[T] {

  /**
   * 'flatMap' or 'bind' function
   *
   * @param f function which contains data from monad - value in container
   * @tparam U new monad type which can be return instead of 'T'
   * @return new monad
   */
  def flatMap[U](f: T => Monad[U]): Monad[U]

  /**
   * Monad creation function
   * Examples of 'unit' function:
   * 'Some(x)' for 'Option' monad
   * 'List(x)' for 'List' monad
   *
   * @param x
   * @tparam T
   * @return
   */
  def unit[T](x: T): Monad[T]
}
