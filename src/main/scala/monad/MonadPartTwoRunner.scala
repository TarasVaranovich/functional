package monad

object MonadPartTwoRunner extends App {

  val monad: Option[Int] = Some(5)
  val monadByMap = monad.map(squareFunction)
  val monadByFlatMapAndUnit = monad.flatMap(x => Some(squareFunction(x)))

  println("Each monad can be represent bt map or unit + flatMap:")
  println(monadByMap == monadByFlatMapAndUnit)

  def squareFunction(x: Int): Option[Int] = Some(x * x)

  def incrementFunction(x: Int): Option[Int] = Some(x + 1)

  println("Left unit law")
  val x = 5
  val monadInteger: Option[Int] = Some(x)
  val monadIntegerByFlatMap = monadInteger.flatMap(squareFunction)
  val functionResult = squareFunction(x)
  println(monadIntegerByFlatMap == functionResult)

  println("Associativity law")
  val left = monadInteger.flatMap(squareFunction).flatMap(incrementFunction)
  val right = monadInteger.flatMap(x => squareFunction(x).flatMap(incrementFunction))
  println(left == right)
}
