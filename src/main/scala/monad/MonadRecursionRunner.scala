package monad

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, Future}
import scala.language.postfixOps

object MonadRecursionRunner extends App {
  //returns stackoverflow after 20000
  println(s"Recursion: ${sum((1 to 2000).toList)}")
  println(s"Tail recursion: ${sum((1 to 20000).toList, 0)}")
  //returns stackoverflow after 2000
  println(s"Option recursion: ${sum((1 to 200).map(Option(_)).toList)}")
  //returns stackoverflow after 2000 or Some(0)
  println(s"Option tail recursion: ${sum((1 to 200).map(Option(_)).toList, 0)}")
  println(s"Future tail recursion: ${Await.result(sum((1 to 20000).map(Future(_)).toList, 0), 3 seconds)}")
  println(s"Tailrec recursion ${sumWithTrampolines((1 to 20000).toList)}")
  println(s"Tailrec option recursion ${sumWithTrampolines((1 to 20000).map(Option(_)).toList)}")

}
