import scala.annotation.tailrec
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

package object monad {

  def squareFunction(value: Int): Int = {
    value * value
  }

  def sum(numbers: List[Int]): Long = numbers match {
    case Nil => 0
    case head :: tail => head + sum(tail)
  }

  def sumWithTrampolines(numbers: List[Int]): Long = {
    import scala.util.control.TailCalls._
    def sumWithTailRec(numbers: List[Int]): TailRec[Long] = numbers match {
      case Nil => done(0)
      case head :: tail => tailcall {
        sumWithTailRec(tail).map(_ + head)
      }
    }

    sumWithTailRec(numbers).result
  }

  @tailrec
  def sum(numbers: List[Int], accum: Long = 0): Long = numbers match {
    case Nil => accum
    case head :: tail => sum(tail, accum + head)
  }

  def sum(numbers: List[Option[Int]]): Option[Long] = numbers match {
    case Nil => Some(0)
    case head :: tail => for {
      h <- head
      tailSum <- sum(tail)
    } yield h + tailSum
  }

  //@tailrec
  def sum(numbers: List[Option[Int]], accum: Long): Option[Long] = numbers match {
    case Nil => Some(0)
    case head :: tail => head.flatMap(h => sum(tail, accum + h))
  }

  def sumWithTrampolines(numbers: List[Option[Int]]): Option[Long] = {
    import scala.util.control.TailCalls._

    def sumWithTailRec(numbers: List[Option[Int]]): TailRec[Option[Long]] = numbers match {
      case Nil => done(Some(0))
      case head :: tail => tailcall {
        sumWithTailRec(tail).map {
          resultOpt => {
            for {
              h <- head
              result <- resultOpt
            } yield result + h
          }
        }
      }
    }

    sumWithTailRec(numbers).result
  }

  def sum(numbers: List[Future[Int]], accum: Long): Future[Long] = numbers match {
    case Nil => Future.successful(accum)
    case head :: tail => head.flatMap(h => sum(tail, accum + h))

  }
}
