package partial_function

import immutability.FunctionalObject

object PartialFunction {

  def main(args: Array[String]): Unit = {

    def multiplyCounter(firstTerm: FunctionalObject, secondTerm: FunctionalObject, thirdTerm: FunctionalObject): Int = {
      firstTerm.counter_ * secondTerm.counter_ * thirdTerm.counter_
    }

    val firstObject = new FunctionalObject(1, "first object")
    val secondObject = new FunctionalObject(2, "second object")
    val thirdObject = new FunctionalObject(3, "third object")

    val entirelyParametrizedFunctionalValue = multiplyCounter(firstObject, secondObject, thirdObject)
    println("Entirely parametrized function execution result:")
    println(entirelyParametrizedFunctionalValue)

    val notParametrizedFunctionalValue = multiplyCounter _
    println("Not parametrized function execution result:")
    println(notParametrizedFunctionalValue)

    val fourthObject = new FunctionalObject(4, "fourth object")
    val fifthObject = new FunctionalObject(5, "fifth object")
    println("Not parametrized function execution result after parameters set:")
    println(notParametrizedFunctionalValue.apply(fourthObject, fifthObject, fifthObject))

    val partialParametrizedFunctionalValue = multiplyCounter(firstObject, _: FunctionalObject, thirdObject)
    println("Partial parametrized function execution result:")
    println(partialParametrizedFunctionalValue)

    println("Partial parametrized function execution result after parameters set:")
    println(partialParametrizedFunctionalValue(fifthObject))

  }
}
