package pure_function

import immutability.FunctionalObject

object FunctionsHandler {

  def summarize(firstTerm: FunctionalObject, secondTerm: FunctionalObject): FunctionalObject = {

    new FunctionalObject(firstTerm.counter_ + secondTerm.counter_,
      String.format("'%s' plus '%s'", firstTerm.message_, secondTerm.message_))
  }

  def sideEffectFunction(): Unit = {
    println("Not a pure function. Just side effect.")
  }

  /**
   * Higher order function which accept first class function as parameter
   * and returns first class function as result.
   *
   * @param changeCounter first-class function
   * @return first-class function
   */
  def performFirstClassFunction(changeCounter: (Int, FunctionalObject) => FunctionalObject):
  (FunctionalObject, FunctionalObject) => Int
  = {

    val summarizeWithCoefficient = (firstObject: FunctionalObject, secondObject: FunctionalObject) => {
      val coefficient = changeCounter.apply(secondObject.counter_, firstObject).counter_
      (firstObject.counter_ + secondObject.counter_) * coefficient
    }
    summarizeWithCoefficient;
  }
}
