package pure_function

import immutability.FunctionalObject

object PureFunction {

  def main(args: Array[String]): Unit = {

    val firstTerm = new FunctionalObject(1, "first")
    val secondTerm = new FunctionalObject(2, "second")

    val result = FunctionsHandler.summarize(firstTerm, secondTerm)
    println(result)

    FunctionsHandler.sideEffectFunction();
  }
}
