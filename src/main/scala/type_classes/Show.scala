package type_classes

trait Show[A] {
  def show(value: A): String
}

object Show {

  def show[A](value: A)(implicit sh: Show[A]) = sh.show(value)

  def show2[A: Show](value: A) = implicitly[Show[A]].show(value)

  def apply[A](implicit sh: Show[A]): Show[A] = sh

  def show3[A: Show](value: A) = Show[A].show(value)

  //usage of single abstract method
  implicit val intToShow: Show[Int] = value => s"Show integer $value"

  implicit val stringToShow: Show[String] = new Show[String] {
    override def show(value: String): String = s"Show string $value"
  }

  val stringToShowMod: Show[String] = value => s"Show mod $value"

  implicit class ShowOps[A: Show](value: A) {
    def show = Show[A].show(value)

    def showExp(implicit sh: Show[A]) = sh.show(value)
  }

}

