package type_classes

import type_classes.Show.{ShowOps, intToShow, show, show2, show3, stringToShowMod}

object TypeClassesRunner extends App {
  println(intToShow.show(20))
  println(show(30))
  println(show2(40))
  println(show3(50))
  println(60.show)
  println("EXP".showExp)
  println("EXP".showExp(stringToShowMod))
}
